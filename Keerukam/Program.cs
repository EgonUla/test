﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Keerukam
{
    class Program
    {
        static void Main(string[] args)
        {


            #region hommikused
            //Console.WriteLine("mingi jutt");
            //Console.WriteLine();
            //int x = 7;


            //Console.WriteLine("Muutuja x on {0} ja tema ruut on {1}", x, x*x); // placeholderitega
            //Console.WriteLine($"Muutuja x on {x:F2} ja tema ruut on {x*x:F2}"); // interpoleeritud string

            //// üks variant veel

            //string s;
            //s = String.Format("Muutuja x on {0} ja tema ruut on {1}", x, x * x); Console.WriteLine(s);
            //s = $"Muutuja x on {x:F3} ja tema ruut on {x * x:F3}"; Console.WriteLine(s);

            //Console.Write("Kes Sa oled: ");
            //string nimi = Console.ReadLine();
            //Console.Write("Kui vana Sa oled? ");
            //int vanus = int.Parse(Console.ReadLine());
            //Console.WriteLine($"Tere {nimi}, kas Sa oled {vanus} aastane");

            //string hennuNimi = "Henn Sarv";
            //hennuNimi = hennuNimi.Trim();

            //Console.WriteLine(hennuNimi.ToUpper());
            //Console.WriteLine(hennuNimi.ToLower());
            //Console.WriteLine(hennuNimi.Length);
            //Console.WriteLine(hennuNimi.Substring(0,4));
            #endregion

            //Console.WriteLine("Kes Sa oled? ");
            //string nimi = Console.ReadLine();
            //if (nimi == "Henn")
            //{
            //    Console.WriteLine("Tere Henn!");
            //    Console.WriteLine("Mul on rõõm sind näha");

            //}
            //else
            //{

            //    Console.WriteLine($"no ok - ole siis {nimi}");
            //}


            Console.WriteLine("Mis värvi tuli põleb valgusfooris? ");
            string varv = Console.ReadLine().ToLower();
            //if (varv == "punane")
            //{
            //    Console.WriteLine("Jää seisma!");
            //}
            //else if (varv == "kollane")
            //{
            //    Console.WriteLine("Oota!");
            //}
            //else if (varv == "roheline")
            //{
            //    Console.WriteLine("Sõida edasi!");
            //}
            //else
            //{
            //    Console.WriteLine("See pole värv!");
            //}



            switch (varv)
            {
                case "green":
                    Console.WriteLine("sõida edasi!");
                    break;
                case "yellow":
                    Console.WriteLine("oota!");
                    break;
                case "red":
                    Console.WriteLine("jää seisma!");
                    break;
                default:
                    Console.WriteLine("see pole värv!");
                    break;

            }



        }
    }
}
