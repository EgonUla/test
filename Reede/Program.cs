﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Reede
{
    class Program
    {

        enum Mast { Risti, Ruutu, Ärtu, Poti }

        enum Kaart { Kaks, Kolm, Neli, Viis, Kuus, Seitse, Kaheksa, Üheksa, Kümme, Soldat, Emand, Kuningas, Äss }

        enum Tunnused { punane = 1, Puust = 2, Suur = 4, Kolisev = 8 }


        static void Main(string[] args)
        {













            // Muutujad ja andmetüübid
            // Muutujad on (nimi, tüüp, väärtus, skoop)

            int arv = 7;

            {
                int hobune = 4; // skoop
            }
            {
                string hobune = "suksu";
            }

            // if laused

            if (DateTime.Now.DayOfWeek == DayOfWeek.Friday)
            {
                Console.WriteLine("Täna saab süüa!");
            }
            else
            {
                Console.WriteLine("Täna peab vaatama, kuidas ise hakkama saada");
            }


            // Elvisena

            Console.WriteLine(DateTime.Now.DayOfWeek == DayOfWeek.Friday ? "Täna saab süüa" : "Täna peab hakkama saama");


            // DateTime kasutamine
            DateTime täna = DateTime.Today;
            DateTime henn = new DateTime(1955, 3, 7);
            var vahe = täna - henn;
            Console.WriteLine(vahe.Days);               // henn leatud päevade arv
            Console.WriteLine(vahe.Days * 4 / 1461);        // henn elatud täisaastad
            Console.WriteLine(täna.Year - henn.Year);   // sünniaasta ja tänavuse aasta vahe

            switch (DateTime.Now.DayOfWeek)
            {
                case DayOfWeek.Friday:
                case DayOfWeek.Tuesday:
                    //reedesed ja teisipäevased toimingud
                    break;

                case DayOfWeek.Saturday:
                    //reedesed toimingud
                    break;

                case DayOfWeek.Monday:
                    // esmaspäevased toimingud
                    goto case DayOfWeek.Saturday;

                case DayOfWeek.Wednesday:
                    //reedesed toimingud
                    goto default;

                default:
                    // kõigi muude päevade toimingud
                    break;


            }



            int[] arvud1 = new int[10]; // see on muutuja, mis sisaldab 10 inti (vaikimisi kõik nullid)

            int[] arvud2 = new int[5] { 1, 2, 3, 4, 5 }; // see on muutuja, mis sisaldab 5 inti (1,2,3,4,5)

            int[] arvud3 = { 1, 2, 7 }; // see on muutuja, mis sisaldab 3 inti (1,2,7)

            string[] nimed =
            {
                "Henn",
                "Henn",
                "Henn",
                "Henn",
                "Henn"
            };

            //var inimene = new { Nimi = "Henn", Vanus = 64 };

            //Dictionary<string, int> vanused = new Dictionary<string, int>
            //{
            //     {"Henn", 64 },
            //     {"Henn", 64 },
            //     {"Henn", 64 },
            //     {"Henn", 64 },
            //     {"Henn", 64 }

            //};



            for (int rebane = 0; rebane < 10; rebane++)
            {
                Console.WriteLine($"rebane hüppab {rebane+1}. korda");
            }


            string testSplit = "Henn on ilus poiss";    // string muutuja
            string[] splititud = testSplit.Split(' ');  // kui selle ota kirjutada split funktsioon
                                                        // tulemuseks stringimassiv
            Console.WriteLine(splititud[1]);

            splititud[0] = "Ants";

            testSplit = string.Join(" ", splititud);    // teeb tükeldatud massiivi tagasi stringiks

            Console.WriteLine(testSplit);


            // int.Parse("4723");      // teisendab Teksti arvuks
            // Convert.ToInt32("2333"); // teeb sama asja!


           // Console.Write("Nimi ja palk");
            // palk = int.Parse(Console.ReadLine());
            //int palk;
            //if (int.TryParse(Console.ReadLine(), out palk))
            //{

            //}
            //else
            //{

            //    Console.WriteLine("ise oled");
            //}

            








        }



    }
}
